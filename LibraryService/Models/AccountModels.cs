﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryService.Models
{
    public class LoginModel
    {
        [Required]
        [DisplayName("E-mail")]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Пароль")]
        public string Password { get; set; }
        [DisplayName("Запомнить меня")]
        public bool Remember { get; set; }
    }
    public class ChangePassModel
    {
        [Required]
        [DisplayName("Старый пароль")]
        [DataType(DataType.Password)]
        public string OldPass { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "Слишком короткий пароль")]
        [MaxLength(100, ErrorMessage = "Слишком длинный пароль")]
        [DisplayName("Новый пароль")]
        public string NewPass { get; set; }
        [Required]
        [DisplayName("Подтвердите пароль")]
        [Compare("NewPass", ErrorMessage = "Пароли не совпадают")]
        [UIHint("TextBox")]
        public string ConfirmPass { get; set; }
    }
}