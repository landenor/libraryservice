﻿using LibraryService.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using LibraryService.Helpers;
using System.Web;

namespace LibraryService.Models
{
    public class BookModel
    {
        public Guid Id { get; set; }
        [Required]
        [UIHint("TextBox")]
        [DisplayName("Название")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Жанр")]
        public BookGenres Genre { get; set; }
        public string GenreStr => EnumHelper<BookGenres>.GetDisplayValue(Genre);
        [Required]
        [UIHint("TextBox")]
        [DisplayName("Автор")]
        public string Author { get; set; }
        [Required]
        [UIHint("TextBox")]
        [DisplayName("Издатель")]
        public string Publisher { get; set; }
        public bool Taked { get; set; }
        /// <summary>
        /// Окончание бронирования(если книга не забронирована - null)
        /// </summary>
        public DateTime? ReverseEndDate { get; set; }
        public UserModel Client { get; set; }
    }
}