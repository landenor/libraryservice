﻿using LibraryService.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryService.Models
{
    public class SearchModel
    {
        [UIHint("TextBox")]
        [DisplayName("Автор")]
        public string Author { get; set; }
        [DisplayName("Жанр")]
        public BookGenres? Genre { get; set; }
        [UIHint("TextBox")]
        [DisplayName("Издатель")]
        public string Publisher { get; set; }
    }
}