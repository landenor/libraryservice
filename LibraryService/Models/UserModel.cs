﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.Models
{
    public class UserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IEnumerable<RoleModel> Roles { get; set; }
    }
    public class RoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}