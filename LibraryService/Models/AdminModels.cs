﻿using LibraryService.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryService.Models
{
    public class RegisterModel
    {
        [Required]
        [DisplayName("Имя")]
        [UIHint("TextBox")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Фамилия")]
        [UIHint("TextBox")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [DisplayName("E-mail")]
        [UIHint("TextBox")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Пароль")]
        [UIHint("Password")]
        public string Password { get; set; }
        [DisplayName("Тип пользователя")]
        public RegisterUserTypes UserType { get; set; }
    }
    public class AdminChangePassModel
    {
        [Required]
        [EmailAddress]
        [UIHint("TextBox")]
        [DisplayName("E-mail")]
        public string Email { get; set; }
        [Required]
        [UIHint("Password")]
        [DisplayName("Новый пароль")]
        public string NewPassword { get; set; }
    }
}