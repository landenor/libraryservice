﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryService.Attributes.Filters
{
    /// <summary>
    /// Фильтр, который в случае загрузки страницы будет возвращать всегда главную страницу(Html5 History)
    /// </summary>
    public class AjaxOnlyAttribute : FilterAttribute, IAuthorizationFilter
    {
        private string IndexPage { get; }

        public AjaxOnlyAttribute(string indexPage)
        {
            IndexPage = indexPage;
        }
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest() && filterContext.RequestContext.RouteData.Values["action"].ToString() != IndexPage
                && !filterContext.IsChildAction)
            {
                var view = new ViewResult();
                view.ViewName = IndexPage;
                filterContext.Result = view;
            }
        }
    }
}