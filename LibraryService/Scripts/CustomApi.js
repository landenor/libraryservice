﻿//Статусы JSON:
// 0 - Success
// 1 - ValidationError(Обработанная ошибка)
// 2 - Error(Ошибка сервера)
var CustomApi = {
    _alertCloseButton: '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
    _findAntiForgeryToken: function($form){
        return $form.find('input[name="__RequestVerificationToken"]');
    },
    admin: {
        registerUser: function ($btn, $msg, $form) {
            if (!$form.valid()) {
                return false;
            }
            $btn.button('loading');
            return $.ajax({
                url: '/admin/registeruser',
                type: 'post',
                data: $form.serialize(),
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        $msg.removeClass('alert-danger').addClass('alert-success');
                        $msg.html('<p>Пользователь успешно добавлен</p>')
                    } else {
                        var msgStr = '<ul>';
                        resp.data.forEach(function (elem) {
                            msgStr += '<li>' + elem + '</li>';
                        });
                        msgStr += '</ul>';
                        $msg.removeClass('alert-success').addClass('alert-danger');
                        $msg.html('<p>' + resp.message + '</p>');
                    }
                },
                error: function (jqXhr) {
                    $msg.removeClass('alert-success').addClass('alert-danger');
                },
                complete: function () {
                    $msg.show();
                    $msg.addClass('alert')
                    $btn.button('reset');
                }
            });
        },
        removeUser: function ($btn, email) {
            $btn.addClass('loading');
            var tr = $btn.parents('tr')[0];
            return $.ajax({
                url: '/admin/removeuser',
                type: 'post',
                data: {
                    email: email,
                },
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        tr.remove();
                    } else {
                        alert('Ошибка при удалении пользователя');
                    }
                },
                error: function (jqXhr) {
                    alert('Ошибка при удалении пользователя');
                },
                complete: function () {
                    $btn.button('reset');
                }
            });
        },
        changePassword: function ($btn, $msg, $form) {
            if (!$form.valid()) {
                return false;
            }
            $btn.addClass('loading');
            return $.ajax({
                url: '/admin/changepassword',
                type: 'post',
                data: $form.serialize(),
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        $msg.removeClass('alert-danger').addClass('alert-success');
                        $msg.html('Пароль пользователя успешно изменен');
                    } else {
                        var msgStr = '<ul>';
                        resp.data.forEach(function (elem) {
                            msgStr += '<li>' + elem + '</li>';
                        });
                        msgStr += '</ul>';
                        $msg.removeClass('alert-success').addClass('alert-danger');
                        $msg.html(msgStr);
                    }
                },
                error: function (jqXhr) {
                    $msg.removeClass('alert-success').addClass('alert-danger');
                    $msg.html('Ошибка при изменении пароля.');
                },
                complete: function (jqXhr) {
                    $btn.removeClass('loading');
                    $msg.addClass('alert');
                }
            });
        }
    },
    librarian: {
        addBook: function($form, $btn, $msg){
            if (!$form.valid()) {
                return false;
            }
            $btn.addClass('loading');
            return $.ajax({
                url: '/librarian/addbook',
                type: 'post',
                data: $form.serialize(),
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        $msg.removeClass('alert-danger').addClass('alert-success');
                        $msg.html('Книга успешно добавлена');
                    } else {
                        var msgStr = '<ul>';
                        resp.data.forEach(function (elem) {
                            errorsStr += '<li>' + elem + '</li>';
                        });
                        msgStr += '</ul>';
                        $msg.removeClass('alert-success').addClass('alert-danger');
                        $msg.html(msgStr);
                    }
                },
                error: function (jqXhr) {
                    $msg.removeClass('alert-success').addClass('alert-danger');
                    $msg.html('Ошибка при добавлении книги.');
                },
                complete: function (jqXhr) {
                    $btn.removeClass('loading');
                    $msg.addClass('alert');
                }
            });
        },
        removeBook: function($btn, bookId){
            $btn.addClass('loading');
            return $.ajax({
                url: '/librarian/removebook/' + bookId,
                type: 'post',
                success: function (resp) {
                    alert('Успешно');
                },
                error: function (jqXhr) {
                    alert('Ошибка');
                },
                complete: function (jqXhr) {
                    $btn.removeClass('loading');
                }
            });
        },
        getAll: function ($tabContent) {
            var $tbody = $tabContent.find('tbody');
            $tbody.html('Загрузка...');
            return $.ajax({
                url: '/librarian/getallbooks',
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        var tmpl = $.templates('#defBookRowTmpl');
                        $tbody.html(tmpl.render(resp.data));
                    } else {
                        $tbody.html('<div class="alert alert-danger">Ошибка при загрузке книг.</div>');
                    }
                },
                error: function () {
                    $tbody.html('<div class="alert alert-danger">Ошибка при загрузке книг.</div>');
                }
            });
        },
        //Получить забронированные книги
        reversedBooks: function ($tabContent, email) {
            var $tbody = $tabContent.find('tbody');
            $tbody.html('Загрузка...');
            return $.ajax({
                url: '/librarian/reversedbooks',
                data: 'clientEmail=' + email,
                type: 'post',
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        if (resp.data == null || resp.data.length == 0) {
                            $tbody.html('Книги не найдены')
                        } else {
                            var tmpl = $.templates('#bookRowTmpl');
                            $tbody.html(tmpl.render(resp.data));
                        }
                    } else {
                        $tbody.html('Ошибка при получении забронированных книг.');
                    }
                },
                error: function () {
                    $tbody.html('Ошибка при получении забронированных книг.');
                }
            });
        },
        //Получить забранные книги
        takedBooks: function ($tabcontent, email) {
            var $tbody = $tabcontent.find('tbody');
            $tbody.html('Загрузка...');
            return $.ajax({
                url: '/librarian/takedbooks',
                data: 'clientEmail=' + email,
                type: 'post',
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        if (resp.data == null || resp.data.length == 0) {
                            $tbody.html('Книги не найдены')
                        } else {
                            var tmpl = $.templates('#bookRowTmpl');
                            $tbody.html(tmpl.render(resp.data));
                        }
                    } else {
                        $tbody.html('Ошибка при получении выданных книг.');
                    }
                },
                error: function () {
                    $tbody.html('Ошибка при получении выданных книг.');
                }
            });
        },
        getBook: function ($btn, bookId) {
            $btn.addClass('loading');
            return $.ajax({
                url: '/librarian/getbooktoclient',
                type: 'post',
                data: {
                    bookId: bookId,
                },
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        alert('Книга успешно выдана клиенту');
                    } else {
                        alert('Ошибка при выдаче книги.');
                    }
                },
                error: function () {
                    alert('Ошибка при выдаче книги.');
                },
                complete: function () {
                    $btn.removeClass('loading')
                }
            });
        },
        takeBook: function ($btn, bookId) {
            $btn.addClass('loading');
            return $.ajax({
                url: '/librarian/takebookfromclient',
                type: 'post',
                data: 'bookId=' + bookId,
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        alert('Успех.');
                    } else {
                        alert('Ошибка при возвращении книги.');
                    }
                },
                error: function (resp) {
                    alert('Ошибка');
                }
            });
        }
    },
    client: {
        reversedBooks: function ($tabContent) {
            var $tbody = $tabContent.find('tbody');
            $tbody.html('Загрузка...');
            return $.ajax({
                url: '/client/reversedbooks',
                type: 'post',
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        if (resp.data.length == 0) {
                            $tbody.html('Вы не забронировали ни одной книги.')
                        } else {
                            var tmpl = $.templates('#bookRowTmpl');
                            $tbody.html(tmpl.render(resp.data));
                        }
                    } else {
                        $tbody.html('Ошибка при получении забронированных книг.');
                    }
                },
                error: function () {
                    $tbody.html('Ошибка при получении забронированных книг.');
                }
            });
        },
        takedBooks: function ($tabContent) {
            var $tbody = $tabContent.find('tbody');
            $tbody.html('Загрузка...');
            return $.ajax({
                url: '/client/takedbooks',
                type: 'post',
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        if (resp.data.length == 0) {
                            $tbody.html('Вы не забрали ни одной книги.')
                        } else {
                            var tmpl = $.templates('#bookRowTmpl');
                            $tbody.html(tmpl.render(resp.data));
                        }
                    } else {
                        $tbody.html('Ошибка при получении выданных книг.');
                    }
                },
                error: function () {
                    $tbody.html('Ошибка при получении выданных книг.');
                }
            });
        },
        search: function ($btn, $form, $tabContent) {
            var $tbody = $tabContent.find('tbody');
            $btn.addClass('loading');
            return $.ajax({
                url: '/client/search',
                type: 'post',
                data: $form.serialize(),
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        if (resp.data.length == 0) {
                            $tbody.html('Книги не найдены.')
                        } else {
                            var tmpl = $.templates('#searchRowTmpl');
                            $tbody.html(tmpl.render(resp.data));
                        }

                    }
                }
            });
        },
        reverseBook: function ($btn, bookId) {
            $btn.addClass('loading');
            return $.ajax({
                url: '/client/reversebook',
                type: 'post',
                data: {
                    bookId: bookId,
                },
                success: function (resp) {
                    if (resp.status == undefined) {
                        resp = JSON.parse(resp);
                    }
                    if (resp.status == 0) {
                        alert('Книга успешно выдана клиенту');
                    } else {
                        alert('Ошибка при бронировании книги.');
                    }
                },
                error: function () {
                    alert('Ошибка при выдаче книги.');
                },
                complete: function () {
                    $btn.removeClass('loading')
                }
            });
        }
    }
}