﻿var Utilities = {
    initValidation: function () {
        $('.input-validation-error').parents('.form-group').addClass('has-error');
        $('.field-validation-error').addClass('text-danger');

        //unobtrusive
        $('form').each(function (i, form) {
            var formData = $.data(form),
                settings = formData.validator.settings,
                oldErrorPlacement = settings.errorPlacement,
                oldSuccess = settings.success;

            settings.errorPlacement = function (label, element) {
                oldErrorPlacement(label, element);

                label.parents('.form-group').addClass('has-error');
                label.addClass('text-danger');
            };

            settings.success = function (label) {
                label.parents('.form-group').removeClass('has-error');
                oldSuccess(label);
            };
        })
    }
}