﻿using LibraryService.DAL.Core;
using LibraryService.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryService.DAL.Infrastructure;
using System.Threading.Tasks;
using LibraryService.Enums;
using System.Data.Entity;

namespace LibraryService.DAL.Repositories
{
    public class BookRepository : Repository<Book>
    {
        public BookRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
        public Task<List<Book>> FindByAuthorAsync(string author)
        {
            return Table.Where(x => x.Author.Contains(author) || author.Contains(x.Author)).ToListAsync();
        }
        public Task<List<Book>> FindByPuplisherAsync(string publisher)
        {
            return Table.Where(x => x.Publisher.Contains(publisher) || publisher.Contains(x.Publisher)).ToListAsync();
        }
        public Task<List<Book>> FindAsync(string author, BookGenres genre)
        {
            return Table.Where(x => (x.Author.Contains(author) || author.Contains(x.Author)) && x.Genre == genre).ToListAsync();
        }
        public Task<List<Book>> FindAsync(string author, string publisher)
        {
            return Table.Where(x => (x.Author.Contains(author) || author.Contains(x.Author)) 
                && (x.Publisher.Contains(publisher) || publisher.Contains(x.Publisher))).ToListAsync();
        }
        public Task<List<Book>> FindAsync(BookGenres genre)
        {
            return Table.Where(x => x.Genre == genre).ToListAsync();
        }
        public Task<List<Book>> FindAsync(BookGenres genre, string publisher)
        {
            return Table.Where(x => (x.Publisher.Contains(publisher) || publisher.Contains(x.Publisher))
                && x.Genre == genre).ToListAsync();
        }
        public Task<List<Book>> FindAsync(string author, BookGenres genre, string publisher)
        {
            return Table.Where(x => (x.Author.Contains(author) || author.Contains(x.Author)) && x.Genre == genre && 
                (x.Publisher.Contains(publisher) || publisher.Contains(x.Publisher))).ToListAsync();
        }
        public Task<List<Book>> GetReversedByClientAsync(string clientId)
        {
            return Table.Where(x => x.Client.Id == clientId && x.ReverseEndDate != null).Include(x => x.Client.User).ToListAsync();
        }
        public Task<List<Book>> GetTakedByClientAsync(string clientId)
        {
            return Table.Where(book => book.Client.Id == clientId && book.Taked).Include(x => x.Client.User).ToListAsync();
        }
    }
}