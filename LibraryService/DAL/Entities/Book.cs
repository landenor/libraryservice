﻿using LibraryService.DAL.Core;
using LibraryService.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LibraryService.DAL.Entities
{
    public class Book : GuidEntity
    {
        public string Name { get; set; }
        public BookGenres Genre { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        /// <summary>
        /// Окончание бронирования(если книга не забронирована - null)
        /// </summary>
        public DateTime? ReverseEndDate { get; set; }
        /// <summary>
        /// Забрали ли книгу
        /// </summary>
        public bool Taked { get; set; }
        [ForeignKey("Client")]
        public string ClientId { get; set; }
        /// <summary>
        /// Клиент, который забрал или забронировал книгу(null possible)
        /// </summary>
        public Client Client { get; set; }
    }
}