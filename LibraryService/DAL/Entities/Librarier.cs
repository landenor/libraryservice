﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LibraryService.DAL.Entities
{
    public class Librarier
    {
        [Key, ForeignKey("User")]
        public string Id { get; set; }
        public User User { get; set; }
    }
}