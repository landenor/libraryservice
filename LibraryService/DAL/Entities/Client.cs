﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LibraryService.DAL.Entities
{
    public class Client
    {
        [Key, ForeignKey("User")]
        public string Id { get; set; }
        /// <summary>
        /// Забронированные или забранные книги книги
        /// </summary>
        public IList<Book> Books { get; set; }
        public User User { get; set; }
    }
}