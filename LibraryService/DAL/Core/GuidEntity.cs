﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.DAL.Core
{
    public abstract class GuidEntity : IGuidEntity
    {
        public GuidEntity()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
    }
}