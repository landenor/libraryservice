﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.DAL.Core
{
    public interface IGuidEntity
    {
        Guid Id { get; set; }
    }
}