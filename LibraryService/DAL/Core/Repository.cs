﻿using LibraryService.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LibraryService.DAL.Core
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected AppDbContext DbContext { get; }
        protected DbSet<TEntity> Table { get; }

        public Repository(AppDbContext dbContext)
        {
            DbContext = dbContext;
            Table = DbContext.Set<TEntity>();
        }
        public virtual void Add(TEntity entity)
        {
            Table.Add(entity);
            DbContext.SaveChanges();
        }

        public virtual Task AddAsync(TEntity entity)
        {
            Table.Add(entity);
            return DbContext.SaveChangesAsync();
        }

        public virtual void Delete(TEntity entity)
        {
            Table.Remove(entity);
            DbContext.SaveChanges();
        }

        public virtual Task DeleteAsync(TEntity entity)
        {
            Table.Remove(entity);
            return DbContext.SaveChangesAsync();
        }

        public virtual TEntity Get(params object[] keyValues)
        {
            return Table.Find(keyValues);
        }

        public virtual Task<TEntity> GetAsync(params object[] keyValues)
        {
            return Table.FindAsync(keyValues);
        }

        public virtual void Update(TEntity entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public virtual Task UpdateAsync(TEntity entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
            return DbContext.SaveChangesAsync();
        }

        public Task<List<TEntity>> GetAllAsync()
        {
            return Table.ToListAsync();
        }

        public List<TEntity> GetAll()
        {
            return Table.ToList();
        }
    }
}