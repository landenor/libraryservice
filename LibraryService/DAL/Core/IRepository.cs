﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LibraryService.DAL.Core
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(params object[] keyValues);
        TEntity Get(params object[] keyValues);
        Task<List<TEntity>> GetAllAsync();
        List<TEntity> GetAll();
        Task AddAsync(TEntity entity);
        void Add(TEntity entity);
        Task UpdateAsync(TEntity entity);
        void Update(TEntity entity);
        Task DeleteAsync(TEntity entity);
        void Delete(TEntity entity);
    }
}