﻿using LibraryService.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.DAL.Infrastructure
{
    public interface IUnitOfWork
    {
        BookRepository BookRepository { get; }
    }
    public class UnitOfWork : IUnitOfWork
    {
        private AppDbContext DbContext { get; }

        private BookRepository bookRepository;

        public BookRepository BookRepository => bookRepository ?? (bookRepository = new BookRepository(DbContext));

        public UnitOfWork(AppDbContext dbContext)
        {
            DbContext = dbContext;
        }
    }
}