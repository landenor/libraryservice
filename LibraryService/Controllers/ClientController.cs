﻿using LibraryService.BLL.Service;
using LibraryService.Constants;
using LibraryService.Core;
using LibraryService.Enums;
using LibraryService.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryService.Controllers
{
    [Authorize(Roles = UserRoles.Client)]
    public class ClientController : BaseController
    {
        private readonly IClientService clientService;

        public ClientController(IClientService clientService)
        {
            this.clientService = clientService;
        }
        public ViewResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> ReverseBook(Guid bookId)
        {
            var result = await clientService.ReverseBook(bookId, User.Identity.GetUserId());
            if (result.Succeeded)
            {
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.ValidationError, result.Errors);
        }
        [HttpPost]
        public async Task<JsonResult> ReversedBooks()
        {
            var books = await clientService.GetReversedBooksByClient(User.Identity.GetUserId());
            return Json(JsonStatuses.Success, books);
        }
        [HttpPost]
        public async Task<JsonResult> TakedBooks()
        {
            var books = await clientService.GetTakedBooksAsync(User.Identity.GetUserId());
            return Json(JsonStatuses.Success, books);
        }
        public async Task<JsonResult> Search(SearchModel model)
        {
            var books = await clientService.SearchBooks(model.Author, model.Genre, model.Publisher);
            return Json(JsonStatuses.Success, books);
        }
    }
}