﻿using LibraryService.Attributes.Filters;
using LibraryService.BLL.Managers;
using LibraryService.Core;
using LibraryService.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryService.Controllers
{
    public class AccountController : BaseController
    {
        private readonly AppSignInManager signInManager;
        private readonly AppUserManager userManager;

        public AccountController(AppSignInManager signInManager, AppUserManager userManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
        }
        [OnlyGuests]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var model = new LoginModel { Remember = true };
            return View(model);
        }
        [HttpPost]
        [OnlyGuests]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await signInManager.PasswordSignInAsync(model.UserName, model.Password, model.Remember, true);
            switch (result)
            {
                case SignInStatus.Success:
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return new RedirectResult(returnUrl);
                    }
                    return new RedirectResult(Url.Action("Index", "Home"));
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.Remember });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("SignInFail", "Неверная пара логин/пароль");
                    return View(model);
            }
        }
        public async Task<ActionResult> ChangePassword(ChangePassModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await userManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPass, model.NewPass);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Проверьте правильность введенных данных");
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectToRouteResult Logoff()
        {
            signInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }
    }
}