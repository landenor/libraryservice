﻿using LibraryService.BLL.Service;
using LibraryService.Constants;
using LibraryService.Core;
using LibraryService.Models;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryService.Controllers
{
    [Authorize(Roles = UserRoles.Librarian)]
    public class LibrarianController : BaseController
    {
        private readonly ILibrarianService librarierService;

        public LibrarianController(ILibrarianService librarierService)
        {
            this.librarierService = librarierService;
        }
        public ViewResult Index()
        {
            return View();
        }
        public async Task<JsonResult> GetAllBooks()
        {
            var books = await librarierService.GetAllAsync();
            return Json(JsonStatuses.Success, books, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> TakedBooks(string clientEmail)
        {
            var books = await librarierService.GetTakedByClientAsync(clientEmail);
            return Json(JsonStatuses.Success, books, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> ReversedBooks(string clientEmail)
        {
            var books = await librarierService.GetReversedBooksByClientAsync(clientEmail);
            return Json(JsonStatuses.Success, books, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> AddBook(BookModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(JsonStatuses.ValidationError, GetModelErrors());
            }
            var result = await librarierService.AddAsync(model);
            if (result.Succeeded)
            {
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.ValidationError, result.Errors);
        }
        public async Task<JsonResult> RemoveBook(Guid id)
        {
            var result = await librarierService.DeleteAsync(id);
            if (result.Succeeded)
            {
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.ValidationError, result.Errors);
        }
        public async Task<JsonResult> GetBookToClient(Guid bookId)
        {
            var result = await librarierService.GetToClientAsync(bookId);
            if (result.Succeeded)
            {
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.ValidationError, result.Errors, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> TakeBookFromClient(Guid bookId)
        {
            var result = await librarierService.TakeFromClientAsync(bookId);
            if (result.Succeeded)
            {
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.ValidationError, result.Errors);
        }
    }
}