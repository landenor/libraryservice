﻿using AutoMapper;
using LibraryService.Attributes.Filters;
using LibraryService.BLL.Managers;
using LibraryService.Constants;
using LibraryService.Core;
using LibraryService.DAL.Entities;
using LibraryService.Enums;
using LibraryService.Models;
using NLog;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryService.Controllers
{
    [AjaxOnly("Index")]
    [Authorize(Roles = UserRoles.Admin)]
    public class AdminController : BaseController
    {
        private readonly AppUserManager userManager;
        private readonly AppRoleManager roleManager;
        private readonly ILogger logger;

        public AdminController(AppUserManager userManager, AppRoleManager roleManager, ILogger logger)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.logger = logger;
        }
        public async Task<ViewResult> Index()
        {
            var roles = await roleManager.Roles.ToListAsync();

            var users = await userManager.Users.Include(x => x.Roles).ToListAsync();
            var userModels = Mapper.Map<IEnumerable<UserModel>>(users);
            //Находим имена ролей для модели
            foreach(var user in userModels)
            {
                foreach(var userRole in user.Roles)
                {
                    userRole.Name = roles.First(role => role.Id == userRole.Id).Name;
                }
            }
            return View(userModels);
        }
        public ActionResult RegisterUser()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RegisterUser(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(JsonStatuses.ValidationError, GetModelErrors());
            }
            var user = Mapper.Map<User>(model);
            var result = await userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var role = "";
                switch (model.UserType)
                {
                    case RegisterUserTypes.Client:
                        role = UserRoles.Client;
                        break;
                    case RegisterUserTypes.Librarier:
                        role = UserRoles.Librarian;
                        break;
                    case RegisterUserTypes.Admin:
                        role = UserRoles.Admin;
                        break;
                    default:
                        return Json(JsonStatuses.ValidationError, "Неизвестный тип пользователя");
                }
                await userManager.AddToRoleAsync(user.Id, role);
                //await userManager.SendEmailAsync(user.Id, "Вы зарегистрированы в библиотеке.",
                //    "Вы зарегистрированы в нашей библиотеке, ваш пароль:" + model.Password);
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.Error);
        }
        [HttpPost]
        public async Task<JsonResult> RemoveUser(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            var result = await userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                return Json(JsonStatuses.Success);
            }
            return Json(JsonStatuses.Error);
        }
        /// <summary>
        /// Изменить пароль
        /// </summary>
        /// <param name="email">Пользователь, которому надо изменить пароль</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> ChangePassword(AdminChangePassModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(JsonStatuses.ValidationError, GetModelErrors());
            }
            var user = await userManager.FindByEmailAsync(model.Email);
            var result = await userManager.RemovePasswordAsync(user.Id);
            if (result.Succeeded)
            {
                result = await userManager.AddPasswordAsync(user.Id, model.NewPassword);
                if (result.Succeeded)
                {
                    return Json(JsonStatuses.Success);
                }
                logger.Error("Ошибка при добавлении пароля пользователя: ", string.Join(", ", result.Errors));
                return Json(JsonStatuses.Error);
            }
            logger.Error("Ошибка при удалении пароля пользователя: ", string.Join(", ", result.Errors));
            return Json(JsonStatuses.Error);
        }
    }
}