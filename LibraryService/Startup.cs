﻿using System;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using LibraryService.BLL.Managers;
using LibraryService.DAL.Entities;
using System.Data.Entity;
using LibraryService.App_Start;
using Microsoft.Owin.Security.DataProtection;
using LibraryService.DAL.Infrastructure;

[assembly: OwinStartup(typeof(LibraryService.Startup))]
namespace LibraryService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Database.SetInitializer(new DbInitializer(app.GetDataProtectionProvider()));
            var dbContext = new AppDbContext();
            dbContext.Database.Initialize(true);
            var container = AutofacDI.Initalize(app);
            AutoMapperConfig.Initalize();
        }
        private void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/account/login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<AppUserManager, User>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                },
                CookieName = "LibraryCookie",
                LogoutPath = new PathString("/account/logoff")
            });
        }
    }
}