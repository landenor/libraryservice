﻿using Autofac;
using AutoMapper;
using LibraryService.BLL.Managers;
using LibraryService.Constants;
using LibraryService.DAL.Entities;
using LibraryService.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.App_Start
{
    public static class AutoMapperConfig
    {
        public static void Initalize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<RegisterModel, User>()
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));
                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<Client, UserModel>()
                    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
                    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName));
                cfg.CreateMap<IdentityUserRole, RoleModel>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.RoleId));
                cfg.CreateMap<BookModel, Book>();
                cfg.CreateMap<Book, BookModel>();
            });
        }
    }
}