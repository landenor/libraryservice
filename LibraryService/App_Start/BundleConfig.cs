﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace LibraryService.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                "~/Scripts/jquery-3.1.1.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jsrender.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/validation").Include(
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/jsrender").Include(
                "~/bower_components/jsrender/jsrender.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                "~/Scripts/CustomApi.js",
                "~/Scripts/Utilities.js"
            ));
            bundles.Add(new StyleBundle("~/Content/site").Include(
                "~/Content/bootstrap.css",
                "~/Content/Site.css"
            ));
        }
    }
}