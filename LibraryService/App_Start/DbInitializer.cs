﻿using LibraryService.BLL.Managers;
using LibraryService.Constants;
using LibraryService.DAL.Entities;
using LibraryService.DAL.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using System.Data.Entity;

namespace LibraryService.App_Start
{
    public class DbInitializer : CreateDatabaseIfNotExists<AppDbContext>
    {
        IDataProtectionProvider _dataProtectionProvider;

        public DbInitializer(IDataProtectionProvider dataProtectionProvider) : base()
        {
            _dataProtectionProvider = dataProtectionProvider;
        }
        protected override void Seed(AppDbContext context)
        {
            base.Seed(context);

            var userManager = new AppUserManager(new UserStore<User>(context), _dataProtectionProvider);
            var roleManager = new AppRoleManager();

            var adminRole = new IdentityRole { Name = UserRoles.Admin };
            var librarianRole = new IdentityRole { Name = UserRoles.Librarian };
            var clientRole = new IdentityRole { Name = UserRoles.Client };

            roleManager.Create(adminRole);
            roleManager.Create(librarianRole);
            roleManager.Create(clientRole);

            var admin = new User
            {
                Email = "ponchitos16@gmail.com",
                UserName = "ponchitos16@gmail.com",
                FirstName = "Раиль",
                LastName = "Файрушин"
            };
            const string pass = "turbo1631";
            var result = userManager.Create(admin, pass);

            //если создание пользователя успешно
            if (result.Succeeded)
            {
                userManager.AddToRoles(admin.Id, adminRole.Name, clientRole.Name, librarianRole.Name);
            }
        }
    }
}