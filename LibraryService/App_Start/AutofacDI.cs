﻿using Autofac;
using Autofac.Integration.Mvc;
using LibraryService.BLL.Managers;
using LibraryService.BLL.Service;
using LibraryService.DAL.Entities;
using LibraryService.DAL.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using NLog;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace LibraryService.App_Start
{
    public class AutofacDI
    {
        public static IContainer Initalize(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            var executingAssembly = Assembly.GetExecutingAssembly();

            builder.RegisterControllers(executingAssembly);
            RegisterComponents(builder, app);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            return container;
        }
        private static void RegisterComponents(ContainerBuilder builder, IAppBuilder app)
        {
            var dataProtectionProvider = app.GetDataProtectionProvider();
            builder.RegisterType<AppDbContext>().InstancePerRequest();
            builder.Register(c => new AppUserManager(new UserStore<User>(c.Resolve<AppDbContext>()),
                dataProtectionProvider));
            builder.Register(c => c.Resolve<IOwinContext>().Authentication)
                .As<IAuthenticationManager>().InstancePerRequest();
            builder.RegisterType<AppSignInManager>();
            builder.RegisterType<AppRoleManager>();

            builder.Register(c => LogManager.GetLogger("LOGGER")).As<ILogger>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            builder.RegisterType<LibrarianService>().As<ILibrarianService>();
            builder.RegisterType<ClientService>().As<IClientService>();
        }
    }
}