﻿using Microsoft.AspNet.Identity;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace LibraryService.UserServices
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            var from = "dispatch@libservice.ru";
            var to = message.Destination;
            var client = new SmtpClient("smtp.yandex.ru", 25)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(from, "qwerty123"),
                EnableSsl = true
            };
            var mail = new MailMessage(new MailAddress(from, "LibService"), new MailAddress(to))
            {
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = true
            };
            return client.SendMailAsync(mail);
        }
    }
}