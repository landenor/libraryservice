﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.Constants
{
    public static class UserRoles
    {
        public const string Admin = "admin";
        public const string Client = "client";
        public const string Librarian = "librarian";
    }
}