﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryService.Enums
{
    public enum RegisterUserTypes
    {
        /// <summary>
        /// Библиотекарь
        /// </summary>
        [Display(Name = "Библиотекарь")]
        Librarier,
        /// <summary>
        /// Клиент
        /// </summary>
        [Display(Name = "Клиент")]
        Client,
        /// <summary>
        /// Админ
        /// </summary>
        [Display(Name = "Админ")]
        Admin
    }
}