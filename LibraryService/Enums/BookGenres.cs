﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryService.Enums
{
    public enum BookGenres
    {
        [Display(Name = "Классика")]
        Classic,
        [Display(Name = "Фантастика")]
        Fantastic,
        [Display(Name = "Роман")]
        Romance,
        [Display(Name = "Детектив")]
        Detective,
        [Display(Name = "Ужасы")]
        Horror
    }
}