// <auto-generated />
namespace LibraryService.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddClientId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddClientId));
        
        string IMigrationMetadata.Id
        {
            get { return "201707131951009_AddClientId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
