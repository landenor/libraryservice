namespace LibraryService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBooksClient : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Books", new[] { "Client_Id1" });
            DropColumn("dbo.Books", "Client_Id");
            RenameColumn(table: "dbo.Books", name: "Client_Id1", newName: "Client_Id");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Books", name: "Client_Id", newName: "Client_Id1");
            AddColumn("dbo.Books", "Client_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Books", "Client_Id1");
        }
    }
}
