namespace LibraryService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClientId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Books", name: "Client_Id", newName: "ClientId");
            RenameIndex(table: "dbo.Books", name: "IX_Client_Id", newName: "IX_ClientId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Books", name: "IX_ClientId", newName: "IX_Client_Id");
            RenameColumn(table: "dbo.Books", name: "ClientId", newName: "Client_Id");
        }
    }
}
