﻿using LibraryService.BLL.Core;
using LibraryService.Enums;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryService.DAL.Infrastructure;
using NLog;
using AutoMapper;
using LibraryService.DAL.Entities;
using System.Linq;

namespace LibraryService.BLL.Service
{
    public interface IClientService
    {
        Task<IEnumerable<BookModel>> GetReversedBooksByClient(string clientId);
        Task<IEnumerable<BookModel>> GetTakedBooksAsync(string clientId);
        /// <summary>
        /// Забронировать книгу
        /// </summary>
        /// <returns></returns>
        Task<DataServiceResult> ReverseBook(Guid bookId, string clientId);
        /// <summary>
        /// Искать книгу
        /// </summary>
        /// <param name="author"></param>
        /// <param name="genre"></param>
        /// <param name="publisher"></param>
        /// <returns></returns>
        Task<IEnumerable<BookModel>> SearchBooks(string author, BookGenres? genre, string publisher);
    }
    public class ClientService : DataService, IClientService
    {
        public ClientService(IUnitOfWork unitOfWork, ILogger logger) : base(unitOfWork, logger)
        {
        }

        public async Task<IEnumerable<BookModel>> GetReversedBooksByClient(string clientId)
        {
            var books = await UnitOfWork.BookRepository.GetReversedByClientAsync(clientId);
            return Mapper.Map<IEnumerable<BookModel>>(books);
        }

        public async Task<IEnumerable<BookModel>> GetTakedBooksAsync(string clientId)
        {
            var books = await UnitOfWork.BookRepository.GetTakedByClientAsync(clientId);
            return Mapper.Map<IEnumerable<BookModel>>(books);
        }

        public async Task<DataServiceResult> ReverseBook(Guid bookId, string clientId)
        {
            var repo = UnitOfWork.BookRepository;
            var book = await repo.GetAsync(bookId);
            book.ClientId = clientId;
            //Книга бронируется на 3 дня
            book.ReverseEndDate = DateTime.UtcNow.AddDays(3);
            try
            {
                await repo.UpdateAsync(book);
                return DataServiceResult.Success();
            }
            catch(Exception e)
            {
                Logger.Error("Ошибка при бронировании книги: " + e.Message);
                return DataServiceResult.Failed("Ошибка при бронировании книги.");
            }
        }

        public async Task<IEnumerable<BookModel>> SearchBooks(string author, BookGenres? genre, string publisher)
        {
            var repo = UnitOfWork.BookRepository;
            if(string.IsNullOrEmpty(author) && genre == null && string.IsNullOrEmpty(publisher))
            {
                return null;
            }
            var books = new List<Book>();
            if (!string.IsNullOrEmpty(author) && genre != null && publisher != null)
            {
                books = await repo.FindAsync(author, (BookGenres)genre, publisher);
            }
            else if (!string.IsNullOrEmpty(author) && !string.IsNullOrEmpty(publisher))
            {
                books = await repo.FindAsync(author, publisher);
            }
            else if(genre != null && !string.IsNullOrEmpty(publisher))
            {
                books = await repo.FindAsync((BookGenres)genre, publisher);
            }
            else if(genre != null && !string.IsNullOrEmpty(author))
            {
                books = await repo.FindAsync((BookGenres)genre, author);
            }
            else if(genre != null)
            {
                books = await repo.FindAsync((BookGenres)genre);
            }
            else if (!string.IsNullOrEmpty(author))
            {
                books = await repo.FindByAuthorAsync(author);
            }
            else if (!string.IsNullOrEmpty(publisher))
            {
                books = await repo.FindByPuplisherAsync(publisher);
            }
            return Mapper.Map<IEnumerable<BookModel>>(books);
        }
    }
}