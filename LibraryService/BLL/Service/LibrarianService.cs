﻿using LibraryService.BLL.Core;
using LibraryService.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryService.DAL.Infrastructure;
using AutoMapper;
using LibraryService.DAL.Entities;
using NLog;
using LibraryService.BLL.Managers;

namespace LibraryService.BLL.Service
{
    public interface ILibrarianService
    {
        /// <summary>
        /// Добавить книгу
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        Task<DataServiceResult> AddAsync(BookModel book);
        /// <summary>
        /// Удалить книгу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DataServiceResult> DeleteAsync(Guid id);
        /// <summary>
        /// Выдать книгу клиенту
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<DataServiceResult> GetToClientAsync(Guid id);
        /// <summary>
        /// Принять книгу
        /// </summary>
        /// <param name="id"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<DataServiceResult> TakeFromClientAsync(Guid id);
        /// <summary>
        /// Выдать забронированные книги 
        /// </summary>
        /// <param name="clientEmail">Кто забронировал</param>
        /// <returns></returns>
        Task<IEnumerable<BookModel>> GetReversedBooksByClientAsync(string clientEmail);
        /// <summary>
        /// Выдать забранные книги
        /// </summary>
        /// <param name="clientEmail"></param>
        /// <returns></returns>
        Task<IEnumerable<BookModel>> GetTakedByClientAsync(string clientEmail);
        Task<IEnumerable<BookModel>> GetAllAsync();
    }
    public class LibrarianService : DataService, ILibrarianService
    {
        private readonly AppUserManager userManager;

        public LibrarianService(IUnitOfWork unitOfWork, ILogger logger, AppUserManager userManager) 
            : base(unitOfWork, logger)
        {
            this.userManager = userManager;
        }

        public async Task<DataServiceResult> AddAsync(BookModel book)
        {
            var bookEntity = Mapper.Map<Book>(book);
            bookEntity.Id = Guid.NewGuid();
            try
            {
                await UnitOfWork.BookRepository.AddAsync(bookEntity);
            }
            catch(Exception e)
            {
                Logger.Error("Ошибка при добавлении книги: " + e.Message);
                return DataServiceResult.Failed("Ошибка при добавлении книги.");
            }
            return DataServiceResult.Success();
        }

        public async Task<DataServiceResult> DeleteAsync(Guid id)
        {
            var book = await UnitOfWork.BookRepository.GetAsync(id);
            try
            {
                await UnitOfWork.BookRepository.DeleteAsync(book);
            }
            catch(Exception e)
            {
                Logger.Error("Ошибка при удалении книги: " + e.Message);
                return DataServiceResult.Failed("Ошибка при удалении книги.");
            }
            return DataServiceResult.Success();
        }

        public async Task<IEnumerable<BookModel>> GetAllAsync()
        {
            var books = await UnitOfWork.BookRepository.GetAllAsync();
            var booksModels = Mapper.Map<IEnumerable<BookModel>>(books);
            return booksModels;
        }

        public async Task<IEnumerable<BookModel>> GetReversedBooksByClientAsync(string clientEmail)
        {
            var client = await userManager.FindByEmailAsync(clientEmail);
            var result = await UnitOfWork.BookRepository.GetReversedByClientAsync(client.Id);
            return Mapper.Map<IEnumerable<BookModel>>(result);
        }

        public async Task<IEnumerable<BookModel>> GetTakedByClientAsync(string clientEmail)
        {
            var client = await userManager.FindByEmailAsync(clientEmail);
            var books = await UnitOfWork.BookRepository.GetTakedByClientAsync(client.Id);
            return Mapper.Map<IEnumerable<BookModel>>(books);
        }

        public async Task<DataServiceResult> GetToClientAsync(Guid id)
        {
            var book = await UnitOfWork.BookRepository.GetAsync(id);
            book.ReverseEndDate = null;
            book.Taked = true;
            try
            {
                await UnitOfWork.BookRepository.UpdateAsync(book);
            }
            catch(Exception e)
            {
                Logger.Error("Ошибка при выдачи книги клиенту: " + e.Message);
                return DataServiceResult.Failed("Ошибка при выдаче книги.");
            }
            return DataServiceResult.Success();
        }

        public async Task<DataServiceResult> TakeFromClientAsync(Guid id)
        {
            var book = await UnitOfWork.BookRepository.GetAsync(id);

            book.Taked = false;
            book.ClientId = null;
            try
            {
                await UnitOfWork.BookRepository.UpdateAsync(book);
            }
            catch(Exception e)
            {
                Logger.Error("Ошибка при принятии книги от клиента: " + e.Message);
                return DataServiceResult.Failed("Ошибка.");
            }
            return DataServiceResult.Success();
        }
    }
}