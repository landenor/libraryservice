﻿using LibraryService.DAL.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryService.BLL.Core
{
    public abstract class DataService
    {
        protected IUnitOfWork UnitOfWork { get; }
        protected ILogger Logger { get; }

        protected DataService(IUnitOfWork unitOfWork, ILogger logger)
        {
            UnitOfWork = unitOfWork;
            Logger = logger;
        }
    }
    public sealed class DataServiceResult
    {
        private DataServiceResult(IEnumerable<string> errors)
        {
            Succeeded = false;
            Errors = errors.ToList();
        }
        private DataServiceResult(bool success, object data)
        {
            Succeeded = success;
            ResultData = data;
        }

        public object ResultData { get; }
        public bool Succeeded { get; }
        public IEnumerable<string> Errors { get; }

        public static DataServiceResult Success()
        {
            return Success(null);
        }
        public static DataServiceResult Success(object data)
        {
            return new DataServiceResult(true, data);
        }
        public static DataServiceResult Failed(params string[] errors)
        {
            return new DataServiceResult(errors);
        }
        public static DataServiceResult Failed(IEnumerable<string> errors)
        {
            return new DataServiceResult(errors);
        }
        public static DataServiceResult Failed(object data, params string[] errors)
        {
            return new DataServiceResult(false, data);
        }
        public static DataServiceResult Failed(object data, IEnumerable<string> errors)
        {
            return new DataServiceResult(false, data);
        }
    }
}